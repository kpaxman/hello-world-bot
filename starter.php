<?php
// List selected from list at http://www.roesler-ac.de/wolfram/hello.htm
$hello_world = array('Hallo, wêreld!', 'Pershëndetje Botë', 'Salam Dünya', 'Ahoj Světe!', 'Kaixo mundua!', 'Shani Mwechalo!', 'Shagatam Prithivi!', 'Zdravo Svijete!', 'Hola món!', 'Klahowya Hayas Klaska', 'Bok Svijete!', 'Hej, Verden!', 'Hallo, wereld!', 'Hello World!', 'Saluton mondo!', 'Tere maailm!', 'Hei maailma!', 'Salut le Monde!', 'Hallo, wrâld!', 'Ola mundo!', 'Hallo Welt!', 'Aloha Honua', 'Nyob zoo ntiaj teb.', 'Helló világ!', 'Halló heimur!', 'Halo Dunia!', 'Dia dhaoibh, a dhomhain!', 'Ciao Mondo!', 'Habari dunia!', 'Niatia thi!', 'nuqneH', 'AVE MVNDE', 'Sveika, Pasaule!', 'Sveikas, Pasauli', 'Moien Welt!', 'Manao ahoana ry tany!', 'Namaskaram, lokame', 'Merhba lid-dinja', 'Hallo verden!', 'Witaj świecie!', 'Olá, mundo!', 'Salut lume!', 'Zdravo Svete!', 'Ahoj, svet!', 'Pozdravljen svet!', '¡Hola mundo!', 'Hejsan världen!', 'Kamusta mundo!', 'Merhaba Dünya!', 'Xin chào thế giới', 'S\'mae byd!', 'Sawubona Mhlaba');

/*

When a matching chat message is received, a POST will be sent to the URL(s) specified below. The data is defined as follows:

token=2dSrxeDaROya1lDAvmLjJRq0
team_id=T0001
team_domain=example
channel_id=C2147483705
channel_name=test
timestamp=1355517523.000005
user_id=U2147483697
user_name=Steve
text=googlebot: What is the air-speed velocity of an unladen swallow?
trigger_word=googlebot:

Please note that the content of message attachments will not be included in the outgoing POST data.

Text must be returned in JSON format. Formatting help: https://api.slack.com/docs/formatting

*/
