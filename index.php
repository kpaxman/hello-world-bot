<?php
// List selected from list at http://www.roesler-ac.de/wolfram/hello.htm
$hello_world = array('Hallo, wêreld!', 'Pershëndetje Botë', 'Salam Dünya', 'Ahoj Světe!', 'Kaixo mundua!', 'Shani Mwechalo!', 'Shagatam Prithivi!', 'Zdravo Svijete!', 'Hola món!', 'Klahowya Hayas Klaska', 'Bok Svijete!', 'Hej, Verden!', 'Hallo, wereld!', 'Hello World!', 'Saluton mondo!', 'Tere maailm!', 'Hei maailma!', 'Salut le Monde!', 'Hallo, wrâld!', 'Ola mundo!', 'Hallo Welt!', 'Aloha Honua', 'Nyob zoo ntiaj teb.', 'Helló világ!', 'Halló heimur!', 'Halo Dunia!', 'Dia dhaoibh, a dhomhain!', 'Ciao Mondo!', 'Habari dunia!', 'Niatia thi!', 'nuqneH', 'AVE MVNDE', 'Sveika, Pasaule!', 'Sveikas, Pasauli', 'Moien Welt!', 'Manao ahoana ry tany!', 'Namaskaram, lokame', 'Merhba lid-dinja', 'Hallo verden!', 'Witaj świecie!', 'Olá, mundo!', 'Salut lume!', 'Zdravo Svete!', 'Ahoj, svet!', 'Pozdravljen svet!', '¡Hola mundo!', 'Hejsan världen!', 'Kamusta mundo!', 'Merhaba Dünya!', 'Xin chào thế giới', 'S\'mae byd!', 'Sawubona Mhlaba');

/*

When a matching chat message is received, a POST will be sent to the URL(s) specified below. The data is defined as follows:

token=2dSrxeDaROya1lDAvmLjJRq0
team_id=T0001
team_domain=example
channel_id=C2147483705
channel_name=test
timestamp=1355517523.000005
user_id=U2147483697
user_name=Steve
text=googlebot: What is the air-speed velocity of an unladen swallow?
trigger_word=googlebot:

Please note that the content of message attachments will not be included in the outgoing POST data.

Text must be returned in JSON format. Formatting help: https://api.slack.com/docs/formatting

*/

//expected token from Slack's outgoing webhook configuration page
$expected_token = '2dSrxeDaROya1lDAvmLjJRq0';

//completely ignore anything that doesn't have this token
if (isset($_POST['token']) && $_POST['token'] == $expected_token) {
  
  $response = 'Hi.'; //default response
  
  //figure out the user
  $user = '<@' . $_POST['user_id'] . '|' . $_POST['user_name'] . '>';
  
  //figure out the request and clean it up
  $request = substr($_POST['text'], strlen($_POST['trigger_word']));
  $request = trim($request, ",.:;!? \t\n\r\0\x0B");
  $lower_request = strtolower($request);
  
  //handle requests, if they exist
  if ($lower_request !== '') {
    if ($lower_request == 'hello world') {
      //respond with a random "hello world"
      $response = 'One way to say "hello world" is "' . $hello_world[array_rand($hello_world)] . '".';
    } else {
      //respond with an error notice
      $response = 'I\'m sorry, ' . $user . ', I don\'t understand "' . $request .'".';
    }
  }
  
  //format the response
  $full_response['text'] = $response;
  
  //output the response, as long as the user isn't a bot
  if ($_POST['user_name'] != 'slackbot' && $_POST['user_id'] != 'slackbot') {
    echo json_encode($full_response);
  }

}